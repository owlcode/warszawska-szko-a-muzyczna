<?php
    wp_enqueue_script('jquery');
    wp_enqueue_media();

    $edit[0] = 0;

    if(!empty($_POST)) {
        add_banner($_POST);
    }

    if(!empty($_GET)) {
        if($_GET['edit'] == -1 && isset($_GET['del'])) {
            delete_banner($_GET['del']);
        }

        if($_GET['del'] == -1 && isset($_GET['edit'])) {
            $edit = get_edit_data($_GET['edit']);
        }

        if($_GET['up'] != -1 && $_GET['edit'] == -1 && $_GET['del'] == -1) {
           move_up($_GET['up']);
        }
    }
?>
<div class="wrap">
    <h2>Baner główny</h2>

    <form method="post">
        <input type="hidden" name="edit" value="<?php echo $edit[0]; ?>" />
            <label for="image_url">Obrazek</label>
            <input type="text" name="image_url" id="image_url" class="regular-text" value="<?php echo $edit[1]; ?>">
            <input type="button" name="upload-btn" id="upload-btn" value="Dodaj obrazek"><br/>

        <label for="text">Wpisz tekst</label><br/>
        <textarea name="text" style="width:300px;height:200px;"><?php echo $edit[2]; ?></textarea><br/>

        <?php submit_button(); ?>
    </form>
    <div class="list">
        <?php
            get_banner_list();
        ?>
    </div>
</div>
<div>
</div>
<script type="text/javascript">
    jQuery(document).ready(function($){
        $('#upload-btn').click(function(e) {
            e.preventDefault();
            var image = wp.media({
                title: 'Upload Image',
                multiple: false
            }).open()
                .on('select', function(e){
                    var uploaded_image = image.state().get('selection').first();
                    var image_url = uploaded_image.toJSON().url;
                    $('#image_url').val(image_url);
                });
        });
    });
</script>