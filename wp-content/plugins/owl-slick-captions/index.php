<?php
/*
 Plugin Name: Owl Slick Caption
*/



function onStart() {
    global $wpdb;

    $table = $wpdb->prefix . "bannery";
    $charset = $wpdb->get_charset_collate();

    $sql = "CREATE TABLE $table (
            `id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
            `image` TEXT NOT NULL,
            `text` TEXT,
            `pos` INT NOT NULL
          ) $charset;";

    require_once( ABSPATH . 'wp-admin/includes/upgrade.php');
    dbDelta($sql);
}
register_activation_hook(__FILE__, 'onStart');

function onUninstall() {
    $sql = 'DROP TABLE 00066400_owlwp.wsm_bannery';
    require_once( ABSPATH . 'wp-admin/includes/upgrade.php');
    dbDelta($sql);

}
register_uninstall_hook(__FILE__, 'onUninstall');

function addMenu() {
    add_menu_page(
        'Banery',
        'Banery',
        'manage_options',
        'owl-slick-captions/form.php'
    );
}

add_action('admin_menu', 'addMenu');

// UPLOAD ENGINE
function load_wp_media_files() {
    wp_enqueue_media();
}
add_action( 'admin_enqueue_scripts', 'load_wp_media_files' );

function get_banner_list() {
    global $wpdb;
    $table = $wpdb->prefix . 'bannery';

    $res = $wpdb->get_results("SELECT * FROM $table ORDER BY `pos` DESC");

    foreach($res as $row) {
        echo '<div style="width:100%;border-bottom: 1px solid #ccc;margin:5px 0;overflow: hidden;">';
        echo '<div style="100px;float:left;padding: 10px;font-size: 20px;">';
        echo '<a href="'.$_SERVER['REQUEST_URI'].'&del='.$row->id.'&edit=-1&up=-1">usuń</a><br/><br/>';
        echo '<a href="'.$_SERVER['REQUEST_URI'].'&edit='.$row->id.'&del=-1&up=-1">edytuj</a><br/><br/>';
        echo '<a href="'.$_SERVER['REQUEST_URI'].'&edit=-1&del=-1&up='.$row->pos.'">do góry</a><br/></div>';

        echo '<div style="width: 150px;float:left;">';
                echo '<img src="'.$row->image.'" style="width:100%;"/>';
            echo '</div>';
            echo '<div style="width: 600px;float:left;margin-left:20px;">';
                echo $row->text;
            echo '</div>';
        echo '</div>';
    }
}

function get_position_number($t) {
    global $wpdb;
    $table = $wpdb->prefix . 'bannery';

    $res = $wpdb->get_results("SELECT `pos` FROM $table ORDER BY `pos` ASC");

    $number = 0;

    foreach($res as $row) {
        $number = $row->pos + 1;
    }

    return $number;
}

function add_banner($dane) {
    if($dane['edit'] != 0) {
        update_banner($dane);
        return;
    }

    global $wpdb;

    $table = $wpdb->prefix . 'bannery';

    $position = get_position_number($dane['position']);

    $wpdb->insert($table, array(
        'image' => $dane['image_url'],
        'text' => $dane['text'],
        'pos' => $position
    ));
}
function delete_banner($id) {
    $id = (int)$id;
    global $wpdb;
    $table = $wpdb->prefix . 'bannery';

    if($id == -1) return;

    $wpdb->delete($table, array( 'id' => $id ));
}

function update_banner($dane) {
    global $wpdb;
    $table = $wpdb->prefix . 'bannery';

    $data = array(
        'image' => $dane['image_url'],
        'text' => $dane['text']
    );


    $wpdb->update($table, $data, array('id' => $dane['edit']));
}

function get_edit_data($id) {
    $id = (int)$id;
    global $wpdb;
    $table = $wpdb->prefix . 'bannery';

    $res = $wpdb->get_results("SELECT * FROM $table WHERE `id` = $id");

    foreach($res as $row) {
        $data[0] = $row->id;
        $data[1] = $row->image;
        $data[2] = $row->text;
    }

    return $data;
}

function move_up($id) {
    global $wpdb;
    $table = $wpdb->prefix . 'bannery';
    $id = (int)$id;

    $res = $wpdb->get_results("SELECT count(*) as ile FROM $table");

    if($res[0]->ile == ($id + 1)) {
        echo $res[0]->ile.' == '.$id.' + 1 => return<br/>';
        return;
    }

    $next = $id++;

    $wpdb->update($table, array('pos' => -1), array('pos' => $id));
    $wpdb->update($table, array('pos' => $id), array('pos' => $next));
    $wpdb->update($table, array('pos' => $next), array('pos' => -1));
}

function get_baner_images_list() {
    global $wpdb;
    $table = $wpdb->prefix . 'bannery';

    $res = $wpdb->get_results("SELECT `image` FROM $table ORDER BY `pos` DESC");

    foreach($res as $row) {
        echo "<li style=\"background: url('".$row->image."');background-size: cover;\"><div class=\"screen30\"></div></li>";
    }
}

function get_baner_caption_list() {
    global $wpdb;
    $table = $wpdb->prefix . 'bannery';

    $res = $wpdb->get_results("SELECT `text`,`pos` FROM $table ORDER BY `pos` DESC");

    foreach($res as $row) {
        echo '<div class="text-slide">';
        echo stripslashes($row->text);
        echo '</div>';
    }
}
?>