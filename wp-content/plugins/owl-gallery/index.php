<?php
/*
 Plugin Name: Owl WSM Galeria
*/

function create_movie_review() {
    register_post_type( 'galeria',
        array(
            'labels' => array(
                'name' => 'Galerie',
                'singular_name' => 'Galeria',
                'add_new' => 'Dodaj',
                'add_new_item' => 'Dodaj nową galerię',
                'edit' => 'Edytuj',
                'edit_item' => 'Edytuj galerię',
                'new_item' => 'Nowa galeria',
                'view' => 'Wyświetl',
                'view_item' => 'Wyświetl galerię',
                'search_items' => 'Szukaj',
                'parent' => 'Parent Movie Review'
            ),

            'public' => true,
            'menu_position' => 15,
            'supports' => array( 'title', 'editor', 'thumbnail', 'custom-fields' ),
            'taxonomies' => array( '' )
        )
    );
}

function get_gallery_folder($id) {
    global $wpdb;
    $table = $wpdb->prefix . 'postmeta';
    $id = (int)$id;

    $res = $wpdb->get_results("SELECT * FROM $table WHERE `post_id` = $id AND `meta_key` = 'Folder'");

    foreach($res as $row) {
        return $row->meta_value;
    }
}

function show_galleries($name) {
    global $wpdb;
    $table = $wpdb->prefix . 'postmeta';
    $posts = $wpdb->prefix . 'posts';

    $res = $wpdb->get_results("SELECT * FROM $table where `meta_key` = 'Galeria' AND `meta_value` = '$name'");

    foreach($res as $row) {
        $sql = $wpdb->get_results("SELECT * FROM $table WHERE `post_id` = $row->post_id");
        foreach($sql as $param) {
            if($param->meta_key == 'Folder') {
                $folder = $param->meta_value;
            }

            if($param->meta_key == 'Zdjęcie') {
                $photo = $param->meta_value;
            }
        }

        $sql = $wpdb->get_results("SELECT * FROM $posts WHERE `ID` = $row->post_id");

        foreach($sql as $g) {
                display_gallery($g->post_title, $g->post_content, $g->post_name, $folder, $photo);
        }
    }

}

function display_gallery($name, $info, $url, $folder, $photo) {
    echo '<article class="post">';
    echo '<a href="'.site_url().'/galeria/'.$url.'">';
    echo '<img src="'.ABSPATH.'/wp-content/galeria/'.$folder.'/'.$photo.'.JPG" alt="'.$folder.'" />';
    echo '<h2 class="gallery-title">'.$name.'<br/>'.$info.'</h2>';
    echo '</a>';
    echo '</article>';
}

add_action( 'init', 'create_movie_review' );

?>