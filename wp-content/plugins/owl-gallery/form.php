<?php
    if(!empty($_POST)) {
        if(isset($_POST['gallery'])) {
            add_gallery_handle($_POST['gallery']);
        } else
            add_gallery($_POST);
    }
?>
<div class="wrap">
    <h2>Galeria</h2>
    <form method="post">
        <label for="gallery">Dodaj nową galerię:</label><input type="text" name="gallery" id="gallery" placeholder="Nazwa Galerii" />
        <?php submit_button(); ?>
    </form>
    <form method="post">
        <label for="image_url">Folder</label>
        <select name="folder" id="folder">
            <?php get_available_folders(); ?>
        </select><br/>
        <label for="site">Wybierz galerię</label>
        <select name="site" id="site">
            <?php get_galleries_name(); ?>
        </select><br/>
        <label for="image_url">Obrazek główny</label>
        <input type="text" name="image_url" id="image_url" class="regular-text" >
        <input type="button" name="upload-btn" id="upload-btn" value="Dodaj obrazek"><br/>

        <label for="title">Wpisz tytuł</label>
        <input type="text" name="title" id="title" placeholder="Koncert zimowy"/><br/>
        <label for="info">Wpisz podtytuł</label>
        <input type="text" name="info" id="info" placeholder="p. Alina Mleczko - 22.03.2013"/><br/>

        <?php submit_button(); ?>
    </form>
    <?php get_admin_galleries();?>
</div>
<script type="text/javascript">
    jQuery(document).ready(function($){
        $('#upload-btn').click(function(e) {
            e.preventDefault();
            var image = wp.media({
                title: 'Upload Image',
                multiple: false
            }).open()
                .on('select', function(e){
                    var uploaded_image = image.state().get('selection').first();
                    var image_url = uploaded_image.toJSON().url;
                    $('#image_url').val(image_url);
                });
        });
    });
</script>