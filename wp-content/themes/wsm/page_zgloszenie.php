<?php
/* Template Name: Zgłoszenie */

get_header();

while ( have_posts() ) : the_post();

?>

<header class="page-header nauczyciel">
    <div class="page-title nauczyciel">
        <div class="social">
            <a href="https://www.facebook.com/szkolamuzyczna" target="_blank"><i class="fa fa-facebook"></i></a>
            <a href="https://www.youtube.com/user/WarszawskaMuzyczna" target="_blank"><i class="fa fa-youtube-play"></i></a>
            <a href="https://vimeo.com/muzyczna/videos" target="_blank"><i class="fa fa-vimeo"></i></a>
        </div>
        <div class="page-title-text scroll-fade parallax">
            <h1>
                <?php
                    $title = get_the_title();
                    echo $title;
                ?>
            </h1>
        </div>
    </div>
    <ul class="slider teacher">
        <li style="background: url('<?php echo wp_get_attachment_url(get_post_thumbnail_id($post->ID)) ?>');background-size: cover;"><div class="screen30"></div></li>
    </ul>
</header>
<div class="bg-band"></div>
<div class="main-container">
    <article class="post application-form" style="width: 100%;">
        <form>
            <label for="uczen-imie">Imię</label>
            <input type="text" id="uczen-imie" name="uczen-imie" />

            <label for="uczen-nazwisko">Nazwisko</label>
            <input type="text" id="uczen-nazwisko" name="uczen-nazwisko" />

            <label for="uczen-adres">Adres zamieszkania</label>
            <input type="text" id="uczen-adres" name="uczen-adres" />

            <label for="uczen-miasto">Kod pocztowy i miasto</label>
            <input type="text" id="uczen-miasto" name="uczen-miasto" />

            <label for="uczen-telefon">Telefony (komórkowy, stacjonarny)</label>
            <input type="text" id="uczen-telefon" name="uczen-telefon" />

            <label for="uczen-mail">Mail</label>
            <input type="email" id="uczen-mail" name="uczen-mail" />

        </form>
    </article>
    <div class="masonry-grid" style="display: none;">
        <div class="post-space"></div>
        <?php
        $text = get_the_content();

        $text = audio_prepare($text);

        $text = str_replace('[box]', '<article class="post">', $text);
        $text = str_replace('[/box]', '</article>', $text);

        $text = str_replace('[photo]', '<div style="text-align:center;">', $text);
        $text = str_replace('[/photo]', '</div>', $text);

        $text = str_replace('[nauczyciele sekcja="', '<p><strong>', $text);
        $text = str_replace('" flex]', '</strong><div style="display:flex;flex-wrap: wrap;justify-content: center;" class="teacher">', $text);
        $text = str_replace('[/nauczyciele]', '</div></p>', $text);


        echo $text;
        ?>
    </div>
    <?php
    endwhile;

    get_footer();
    ?>
