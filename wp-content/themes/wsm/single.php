<?php
get_header();
?>
    <header class="page-header">
        <div class="page-title">
            <div class="social">
                <a href="https://www.facebook.com/szkolamuzyczna"><i class="fa fa-facebook"></i></a>
                <a href="https://www.youtube.com/user/WarszawskaMuzyczna"><i class="fa fa-youtube-play"></i></a>
                <a href="https://vimeo.com/muzyczna/videos"><i class="fa fa-vimeo"></i></a>
            </div>
            <div class="page-title-text scroll-fade parallax slider-caption">
                <?php get_baner_caption_list(); ?>
            </div>
        </div>
        <ul class="slider">
            <?php get_baner_images_list(); ?>
        </ul>
    </header>
    <script type="text/javascript">
        jQuery('.slider').slick({
            autoplay: true,
            arrows: false,
            fade: true,
            cssEase: 'ease-in-out',
            asNavFor: '.slider-caption',
            autoplaySpeed: 6000,
            swipe: true
        });
        jQuery('.slider-caption').slick({
            autoplay: true,
            arrows: false,
            asNavFor: '.slider',
            cssEase: 'ease',
            fade: true
        });
    </script>
    <div class="bg-band"></div>
    <!--<div class="bg-band-second"></div>-->
<div class="main-container">
    <div class="masonry-grid">
        <div class="post-space"></div>
        <?php
        if(have_posts()) {
            while(have_posts()) {
                the_post();

                echo '<article class="post">';

                if(get_the_title() != '') {
                    echo '<h2>';
                    echo get_the_title();
                    echo '</h2>';
                }

                $text = get_the_content();

                $text = str_replace('[video]', '<div class="video-wrapper">', $text);
                $text = str_replace('[/video]', '</div>', $text);

                $i = 1;
                $text = str_replace('[box]', '</article><article class="post">', $text, $i );

                $text = str_replace('[box]', '<article class="post">', $text);
                $text = str_replace('[/box]', '</article>', $text);

                echo $text;

                echo '</article>';
            }
        }
        ?>
    </div>
<?php
get_footer();
?>