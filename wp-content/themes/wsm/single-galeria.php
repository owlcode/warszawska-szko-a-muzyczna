<?php
get_header();

while ( have_posts() ) : the_post();

?>

<header class="page-header nauczyciel">
    <div class="page-title nauczyciel">
        <div class="social">
            <a href="https://www.facebook.com/szkolamuzyczna" target="_blank"><i class="fa fa-facebook"></i></a>
            <a href="https://www.youtube.com/user/WarszawskaMuzyczna" target="_blank"><i class="fa fa-youtube-play"></i></a>
            <a href="https://vimeo.com/muzyczna/videos" target="_blank"><i class="fa fa-vimeo"></i></a>
        </div>
        <div class="page-title-text scroll-fade parallax">
            <h1>
                <?php
                $title = get_the_title();

                echo $title;
                ?>
            </h1>
            <h2>
                <?php
                $text = get_the_content();
                echo $text;
                ?>
            </h2>
        </div>
    </div>
    <ul class="slider teacher">
        <li style="background: url('<?php echo wp_get_attachment_url(get_post_thumbnail_id($post->ID)) ?>');background-size: cover;"><div class="screen30"></div></li>
    </ul>
</header>
<div class="bg-band"></div>
<div class="main-container" >
    <div class="gallery">
        <?php

            $folder = get_gallery_folder($post->ID);
            
			if(file_exists('/wp-content/galeria/'.$folder)) {
				$dir = opendir('/wp-content/galeria/'.$folder);
			
				while(false !== ($file = readdir($dir)))
					if($file != '.' && $file != '..') {
						echo '<img src="/wp-content/galeria/'.$folder.'/'.$file.'" />';
					}
			} else {
				echo '<h2>Niestety nie znaleźliśmy takiej galerii :(</h2>';	
			}

        ?>
    </div>
<?php
    endwhile;

    get_footer();
?>
