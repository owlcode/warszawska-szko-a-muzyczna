<?php
    get_header();
?>
    <header class="page-header">
        <div class="page-title">
            <div class="social">
                <a href="https://www.facebook.com/szkolamuzyczna" target="_blank"><i class="fa fa-facebook"></i></a>
                <a href="https://www.youtube.com/user/WarszawskaMuzyczna" target="_blank"><i class="fa fa-youtube-play"></i></a>
                <a href="https://vimeo.com/muzyczna/videos" target="_blank"><i class="fa fa-vimeo"></i></a>
            </div>
            <div class="page-title-text scroll-fade parallax slider-caption">
                <?php get_baner_caption_list(); ?>
            </div>
        </div>
        <ul class="slider">
            <?php get_baner_images_list(); ?>
        </ul>
    </header>
    <script type="text/javascript">
        jQuery('.slider').slick({
            autoplay: true,
            arrows: false,
            fade: true,
            speed: 1200,
            asNavFor: '.slider-caption',
            autoplaySpeed: 8000,
            swipe: true
        });
        jQuery('.slider-caption').slick({
            autoplay: true,
            arrows: false,
			speed: 1200,
            asNavFor: '.slider',
            fade: true
        });
    </script>
<div class="bg-band"></div>
<!--<div class="bg-band-second"></div>-->
<div class="main-container">
    <div class="masonry-grid">
        <div class="post-space"></div>
        <?php
            if(have_posts()) {
                while(have_posts()) {
                    the_post();

                    echo '<article class="post">';

                    if(get_post_thumbnail_id($post->ID)) {
                        echo '<img src="'.wp_get_attachment_url(get_post_thumbnail_id($post->ID)).'" />';
                    }

                    if(get_the_title() != '') {
                        echo '<h2>';
                            echo get_the_title();
                        echo '</h2>';
                    }

                    $text = get_the_content();

					$text = audio_prepare($text);
					
                    $text = str_replace('[video]', '<div class="video-wrapper">', $text);
                    $text = str_replace('[/video]', '</div>', $text);

                    echo $text;

                    echo '</article>';
                }
            }
        ?>
    </div>
<?php
    get_footer();
?>