</div>
<footer>
    <nav>
        <ul class="sitemap">
                <li class="page_item page-item-7 page_item_has_children">O szkole
                    <ul class="children">
                        <li class="page_item page-item-9"><a href="http://muzyczna.art.pl/o-szkole/o-nas/">O nas</a></li>
                        <li class="page_item page-item-13"><a href="http://muzyczna.art.pl/o-szkole/sukcesy/">Sukcesy</a></li>
                        <li class="page_item page-item-15"><a href="http://muzyczna.art.pl/o-szkole/media/">Media</a></li>
                    </ul>
                </li>
                <li class="page_item page-item-20 page_item_has_children">Nauka
                    <ul class="children">
                        <li class="page_item page-item-24"><a href="http://muzyczna.art.pl/nauka/nauczyciele/">Nauczyciele</a></li>
                        <li class="page_item page-item-26"><a href="http://muzyczna.art.pl/nauka/plan-zajec/">Plan zajęć</a></li>
                        <li class="page_item page-item-28"><a href="http://muzyczna.art.pl/nauka/rekrutacja/">Rekrutacja</a></li>
                        <li class="page_item page-item-30"><a href="http://muzyczna.art.pl/nauka/oplaty/">Opłaty</a></li>
                    </ul>
                </li>
                <li class="page_item page-item-32"><a href="http://muzyczna.art.pl/galeria/">Galeria</a></li>
                <li class="page_item page-item-38"><a href="http://muzyczna.art.pl/kontakt/">Kontakt</a></li>
    <!--      --><?php //remove_parent_links(); ?>
        </ul>
    </nav>
    <div class="bottom">
        <div class="social">
            <a href="https://www.facebook.com/szkolamuzyczna" target="_blank"><i class="fa fa-facebook"></i></a>
            <a href="https://www.youtube.com/user/WarszawskaMuzyczna" target="_blank"><i class="fa fa-youtube-play"></i></a>
            <a href="https://vimeo.com/muzyczna/videos" target="_blank"><i class="fa fa-vimeo"></i></a>
        </div>
        <div class="links">
            <?php wp_nav_menu( array( 'theme_location' => 'top-menu' ) ); ?>
        </div>
    </div>
    <div class="information">
        <i class="wsm-icon"></i>
        <div class="address">
            Warszawska Szkoła Muzyczna - aleja Komisji Edukacji Narodowej 36, lok. 126, 02-797 Warszawa <br/>
            Warszawska Szkoła Muzyczna - Żytnia 64, 01-156 Warszawa <br/>
        </div>
        <div class="address-mobile">
            <span>Warszawska Szkoła Muzyczna</span>
        </div>
        <div class="createdby" >
				<small><a href="http://www.dawidsowa.pl" target="_blank">created by owlCode</a></small><br/>
        </div>
    </div>
</footer>
</body>
</html>