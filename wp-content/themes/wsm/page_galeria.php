<?php
/* Template Name: Galerie */
get_header();
while (have_posts()) : the_post();
    $text = get_the_content();

    $text = str_replace('[galeria nazwa="', '', $text);
    $gallery = str_replace('"]', '', $text);

//    show_galleries($text);
endwhile;
?>
<div class="space"></div>
<header class="page-header nauczyciel">
    <div class="page-title nauczyciel">
        <div class="social">
            <a href="https://www.facebook.com/szkolamuzyczna" target="_blank"><i class="fa fa-facebook"></i></a>
            <a href="https://www.youtube.com/user/WarszawskaMuzyczna" target="_blank"><i class="fa fa-youtube-play"></i></a>
            <a href="https://vimeo.com/muzyczna/videos" target="_blank"><i class="fa fa-vimeo"></i></a>
        </div>
        <div class="page-title-text scroll-fade parallax">
            <h1>Galeria</h1>
            <?php
                if($gallery != 'Galeria') {
                  echo '<h2>'.$gallery.'</h2>';
                }
            ?>
        </div>
    </div>
    <ul class="slider teacher">
        <li style="background: url('<?php echo wp_get_attachment_url(get_post_thumbnail_id($post->ID)) ?>');background-size: cover;"><div class="screen30"></div></li>
    </ul>
</header>
<div class="bg-band"></div>
<div class="main-container">
    <div class="masonry-grid">
        <div class="post-space"></div>
        <?php
            $args = array ('post_type' => 'galeria');
            $loop = new WP_QUERY($args);

            while ( $loop->have_posts() ) : $loop->the_post();
                $meta = get_post_meta($post->ID);
                $folder = $meta['Folder'][0];
                $photo = $meta['Zdjęcie'][0];
                $metaGaleria = $meta['Galeria'][0];

                echo '<article class="post">';
                echo '<a href="'.site_url().'/galeria/'.$post->post_name.'" >';
                echo '<img src="'.ABSPATH.'/wp-content/galeria/'.$folder.'/'.$photo.'.JPG" alt="'.get_the_title().'" />';
                echo '<h2 class="gallery-title">';
                echo get_the_title();
                echo '<br/>';
                echo get_the_content();
                echo '</h2>';
                echo '</a>';
                echo '</article>';


            endwhile;
        ?>
    </div>
<?php

    get_footer();
?>
