<?php


    wp_enqueue_style( 'style', get_stylesheet_uri() );
    wp_enqueue_style( 'slick', get_template_directory_uri() . '/css/slick.css' );

    function remove_parent_links() {
        $args = array(
            'authors'      => '',
            'child_of'     => 0,
            'date_format'  => get_option('date_format'),
            'depth'        => 0,
            'echo'         => 0,
            'exclude'      => '',
            'include'      => '',
            'link_after'   => '',
            'link_before'  => '',
            'post_type'    => 'page',
            'post_status'  => 'publish',
            'show_date'    => '',
            'sort_column'  => 'menu_order, post_title',
            'sort_order'   => '',
            'title_li'     => '',
            'walker'       => new Walker_Page
        );

        $pages = wp_list_pages($args);
        $pages = explode("page_item_has_children", $pages);
        $i=0;

        foreach($pages as $page) {
            $page = preg_replace('/<a href="(.*)">/','',$page, 1);

            $pages[$i] = $page;
            $i++;
        }

        $pages = implode('page_item_has_children',$pages);
        echo $pages;
    }


    function owl_wsm_setup() {
        add_theme_support( 'post-thumbnails' );
    }
    add_action( 'after_setup_theme', 'owl_wsm_setup' );

    function owl_wsm_menus() {
        register_nav_menus(array(
            'top-menu' => __('Menu górnego paska', 'wsm'),
            'side-menu' => __('Menu boczne', 'wsm')
        ));
    }
    add_action('init', 'owl_wsm_menus');

    function owl_wsm_scripts() {
        wp_enqueue_script( 'jquery');
        wp_enqueue_script( 'slick', get_template_directory_uri() . '/js/slick.min.js');
        wp_enqueue_script( 'grid', get_template_directory_uri() . '/js/masonry.js');
        wp_enqueue_script( 'owl', get_template_directory_uri() . '/js/owl.js');
    }
    add_action('wp_enqueue_scripts', 'owl_wsm_scripts');

    function audio_prepare($text) {
        $text = str_replace('[audio mp3=', '<audio><source src=', $text);
        $text = str_replace('][/audio]', ' type="audio/mpeg"></audio>', $text);

        $audio = explode('<audio>', $text);

        foreach($audio as $i=>$item) {
            if($i == count($audio)-1) break;
            $audio[$i] = '<i class="fa fa-play-circle audio-'.$i.'" style="font-size: 1.3em;margin-left: 5px;" onclick="musicToggle(\'audio-'.$i.'\')"></i><audio id="audio-'.$i++.'">'.$audio[$i];
        }

        $text = implode('', $audio);

        return $text;
    }

    add_filter( 'the_content_more_link', 'modify_read_more_link' );
    function modify_read_more_link() {
        return '<a class="more-link" href="' . get_permalink() . '">czytaj więcej »</a>';
    }
?>