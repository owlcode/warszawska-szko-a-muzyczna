<?php
get_header();
while ( have_posts() ) : the_post();

?>
<header class="page-header" style="position: relative;">
    <div class="page-title">
        <div class="social">
            <a href="https://www.facebook.com/szkolamuzyczna" target="_blank"><i class="fa fa-facebook"></i></a>
            <a href="https://www.youtube.com/user/WarszawskaMuzyczna" target="_blank"><i class="fa fa-youtube-play"></i></a>
            <a href="https://vimeo.com/muzyczna/videos" target="_blank"><i class="fa fa-vimeo"></i></a>
        </div>
        <div class="page-title-text scroll-fade parallax">
            <?php
            the_title( '<h1>', '</h1>' );
            ?>
        </div>
    </div>
<!--    <ul class="slider parallax-2" style="transition: none;">-->
    <ul class="slider" style="transition: none;">
        <li style="background: url('<?php echo wp_get_attachment_url(get_post_thumbnail_id($post->ID)) ?>');background-size: cover;"><div class="screen30"></div></li>
    </ul>
</header>
<div class="bg-band"></div>
<!--<div class="bg-band-second"></div>-->
<div class="main-container">
    <div class="masonry-grid">
        <div class="post-space"></div>
        <?php
        $text = get_the_content();

        $text = audio_prepare($text);

        $text = str_replace('[box]', '<article class="post">', $text);
        $text = str_replace('[/box]', '</article>', $text);

        $text = str_replace('[photo]', '<div style="text-align:center;">', $text);
        $text = str_replace('[/photo]', '</div>', $text);

        $text = str_replace('[video]', '<div class="video-wrapper">', $text);
        $text = str_replace('[/video]', '</div>', $text);

        $text = str_replace('[dyplomy]', '<div class="dyplomy">', $text);
        $text = str_replace('[/dyplomy]', '</div>', $text);


        echo $text;
        ?>
    </div>

<?php
    endwhile;
    get_footer();
?>
