<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <?php
        wp_head();
    ?>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href='https://fonts.googleapis.com/css?family=Lato&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    <script src="http://maps.google.com/maps?file=api&v=2&key=ABQIAAAAkk8yrIfzPOfZQcVLuiJUdhT7GSceW3R-7MUGgrLyjjuJvr36vxTIFxixaj7-ZXMbPiBct3h2aG0nAg" type="text/javascript"></script>
    <script src="http://www.muzyczna.art.pl/wp-content/themes/wsm/js/maps.js" type="text/javascript"></script>
	<title>Warszawska Szkoła Muzyczna</title>
	<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-29421435-1', 'auto');
  ga('send', 'pageview');

</script>
</head>
<body>
<nav id="side-nav">
    <i class="menu-close"></i>
    <ul class="side-menu">
<!--        --><?php //remove_parent_links(); ?>
            <li class="page_item page-item-7 page_item_has_children">O szkole
                <ul class="children">
                    <li class="page_item page-item-9"><a href="http://muzyczna.art.pl/o-szkole/o-nas/">O nas</a></li>
                    <li class="page_item page-item-13"><a href="http://muzyczna.art.pl/o-szkole/sukcesy/">Sukcesy</a></li>
                    <li class="page_item page-item-15"><a href="http://muzyczna.art.pl/o-szkole/media/">Media</a></li>
                </ul>
            </li>
            <li class="page_item page-item-20 page_item_has_children">Nauka
                <ul class="children">
                    <li class="page_item page-item-24"><a href="http://muzyczna.art.pl/nauka/nauczyciele/">Nauczyciele</a></li>
                    <li class="page_item page-item-26"><a href="http://muzyczna.art.pl/nauka/plan-zajec/">Plan zajęć</a></li>
                    <li class="page_item page-item-28"><a href="http://muzyczna.art.pl/nauka/rekrutacja/">Rekrutacja</a></li>
                    <li class="page_item page-item-30"><a href="http://muzyczna.art.pl/nauka/oplaty/">Opłaty</a></li>
                </ul>
            </li>
            <li class="page_item page-item-32"><a href="http://muzyczna.art.pl/galeria/">Galeria</a></li>
            <li class="page_item page-item-38"><a href="http://muzyczna.art.pl/kontakt/">Kontakt</a></li>
    </ul>

        <?php //wp_nav_menu( array( 'theme_location' => 'top-menu' ) ); ?>

    <div class="menu-side-links-container">
        <ul id="menu-side-links" class="menu">
            <li id="menu-item-41" class=""><a href="http://www.przedszkole.art.pl/">Warszawskie Przedszkole Muzyczne</a></li>
            <li id="menu-item-42" class=""><a href="http://www.kurs.art.pl/">Letni Kurs Muzyczny</a></li>
        </ul>
    </div>
</nav>
<header id="masthead" class="site-header">
    <div class="masthead-inner">
        <a href="#side-nav" class="nav-trigger"><i class="fa fa-bars"></i></a>
        <div class="masthead-container">
            <div class="logo">
                <!--<a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home">
                    <img src="<?php echo get_template_directory(); ?>/assets/logo-min.png" alt="Szkoła Muzyczna Warszawa" />
                </a>-->
				<a href="http://muzyczna.art.pl/" rel="home">
                    <img src="http://muzyczna.art.pl/wp-content/themes/wsm/assets/logo-min.png" alt="Szkoła Muzyczna Warszawa" />
                </a>
            </div>
            <div class="links">
                <?php wp_nav_menu( array( 'theme_location' => 'top-menu' ) ); ?>
            </div>
        </div>
    </div>
</header>
