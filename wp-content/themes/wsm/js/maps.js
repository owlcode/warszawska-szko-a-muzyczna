//<![CDATA[
var zoomScale = 15;

function initMap() {
    if (GBrowserIsCompatible()) {
        var geocoder = new GClientGeocoder();
        geocoder.getLatLng(
            "Warszawa, Żytnia 64",
            function (point) {
                if (!point) {

                } else {
                    mapaObj = new GMap2(document.getElementById("map_centrum"));

                    // Enable the Earth map type
                    mapaObj.addMapType(G_SATELLITE_3D_MAP);

                    mapaObj.addControl(new GLargeMapControl());
                    var typyMapy = mapaObj.getMapTypes();
                    typyMapy[0].getName = function () {
                        return "Mapa";
                    }
                    typyMapy[1].getName = function () {
                        return "Satelita";
                    }
                    typyMapy[2].getName = function () {
                        return "Hybryda";
                    }
                    mapaObj.addControl(new GMapTypeControl());
                    mapaObj.setCenter(point, zoomScale);

                    // tworzymy marker
                    markerObj = new GMarker(point, new GIcon(G_DEFAULT_ICON));
                    mapaObj.addOverlay(markerObj);
                    markerObj.openInfoWindowHtml('<span style="font-size: 10pt; font-family: Lato"><b>Warszawska Szkoła Muzyczna</b><br>ul. Żytnia 64<br>Warszawa</span>');
                }
            }
        );

        geocoder.getLatLng(
            "Warszawa, Komisji Edukacji Narodowej 36",
            function (point) {
                if (!point) {
                    //alert("Nie udało się zlokalizować wskazanego adresu");
                    //nop
                } else {
                    mapaObj = new GMap2(document.getElementById("map_ursynow"));

                    // Enable the Earth map type
                    mapaObj.addMapType(G_SATELLITE_3D_MAP);

                    mapaObj.addControl(new GLargeMapControl());
                    var typyMapy = mapaObj.getMapTypes();
                    typyMapy[0].getName = function () {
                        return "Mapa";
                    }
                    typyMapy[1].getName = function () {
                        return "Satelita";
                    }
                    typyMapy[2].getName = function () {
                        return "Hybryda";
                    }
                    mapaObj.addControl(new GMapTypeControl());
                    mapaObj.setCenter(point, zoomScale);

                    // tworzymy marker
                    markerObj = new GMarker(point, new GIcon(G_DEFAULT_ICON));
                    mapaObj.addOverlay(markerObj);
                    markerObj.openInfoWindowHtml('<span style="font-size: 10pt; font-family: Lato"><b>Warszawska Szkoła Muzyczna</b><br>Al. KEN 36 lok. 126<br>Warszawa</span>');
                }
            }
        );
    }
}

jQuery(window).ready(function() {
    initMap();
});


//]]>