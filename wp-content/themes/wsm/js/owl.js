var Wsm = {};

Wsm.userEvents = function () {
  jQuery(document)
      .on('click', 'i.fa-bars', function() {
          var body = jQuery('body');

          if(body.hasClass('menu-open')) {
              body.removeClass('menu-open');
              jQuery('.fa-bars').css('opacity', '1');
          } else {
              body.addClass('menu-open');
              jQuery('html').css('overflow-y', 'hidden');
              jQuery('.fa-bars').css('opacity', '0');
          }

          return false;
      })
      .on('click', 'i.menu-close', function() {
          jQuery('body').removeClass('menu-open');
          jQuery('html').css('overflow-y', 'visible');
          jQuery('.fa-bars').css('opacity', '1');
      })
      .on('click', '.page_item_has_children', function() {
          var th = jQuery(this);

          if(th.hasClass('opened')) {
              th.find('ul.children').css('display', 'none');
                th.removeClass('opened');
          } else {
              th.addClass('opened');
              th.find('ul.children').css('display', 'block');
          }
      });

      jQuery(window)
          .scroll(function() {
              topPosition = jQuery(this).scrollTop();

              if(topPosition >= 150) {
                  jQuery('#masthead').addClass('minified');
              } else {
                  jQuery('#masthead').removeClass('minified');
              }
          });
};

Wsm.textEffects = function () {
        var speed = 0.002;

        jQuery(window).scroll(function() {
            var opacity = 1 - ( jQuery(window).scrollTop() * speed);
            jQuery('.scroll-fade').css({opacity: opacity});
        });
};

Wsm.parallax = function () {
      jQuery(window).scroll(function() {
          var yPos = jQuery(window).scrollTop() * (0.25),
              prop = {
                  webkitTransform: 'translateY(' + yPos + 'px)',
                  mozTransform: 'translateY(' + yPos + 'px)',
                  msTransform: 'translateY(' + yPos + 'px)',
                  transform: 'translateY(' + yPos + 'px)',
              };

          jQuery('.parallax').css(prop);

		  var yPos = jQuery(window).scrollTop() * 1,
              prop = {
                  webkitTransform: 'translateY(' + yPos + 'px)',
                  mozTransform: 'translateY(' + yPos + 'px)',
                  msTransform: 'translateY(' + yPos + 'px)',
                  transform: 'translateY(' + yPos + 'px)',
              };

          jQuery('.parallax-2').css(prop);
		  
          var yPos = jQuery(window).scrollTop() * (0.75),
              prop = {
                  webkitTransform: 'translateY(' + yPos + 'px)',
                  mozTransform: 'translateY(' + yPos + 'px)',
                  msTransform: 'translateY(' + yPos + 'px)',
                  transform: 'translateY(' + yPos + 'px)',
              };

          jQuery('.bg-band').css(prop);

          var yPos = jQuery(window).scrollTop() * (0.2) + 700,
              prop = {
                  webkitTransform: 'translateY(' + yPos + 'px)',
                  mozTransform: 'translateY(' + yPos + 'px)',
                  msTransform: 'translateY(' + yPos + 'px)',
                  transform: 'translateY(' + yPos + 'px)',
              };

          jQuery('.bg-band-second').css(prop);
      });
};

Wsm.userEvents();
Wsm.textEffects();
Wsm.parallax();

jQuery(function() {

    var $allVideos = jQuery("iframe[src*='//player.vimeo.com'], iframe[src*='//www.youtube.com'], object, embed"),
        $fluidEl = jQuery(".post");

    $allVideos.each(function() {

        jQuery(this)
            .attr('data-aspectRatio', this.height / this.width)
            .removeAttr('height')
            .removeAttr('width');

    });

    jQuery(window).resize(function() {

        var newWidth = $fluidEl.width();
        $allVideos.each(function() {

            var $el = jQuery(this);
            $el
                .width(newWidth)
                .height(newWidth * $el.attr('data-aspectRatio'));

        });

    }).resize();

});

function musicToggle(id) {
    if(document.getElementById(id).paused) {
        document.getElementById(id).play();
        jQuery('.' + id).addClass('fa-pause-circle');
    } else {
        document.getElementById(id).pause();
        jQuery('.' + id).removeClass('fa-pause-circle');
    }

}

jQuery(window).load(function() {
    //jQuery('img').load(function() {
        jQuery('.masonry-grid').each(function () {

            var grid = jQuery('.masonry-grid').masonry({
                "columnWidth": '.post',
                "gutter": ".post-space",
                "itemSelector": ".post",
                "initLayout": false
            });

            grid.masonry( 'on', 'layoutComplete', function() {
                console.log('layout is complete');
            });

            grid.masonry();
        });
    //});
	 jQuery("footer").css('z-index', '5');
});

