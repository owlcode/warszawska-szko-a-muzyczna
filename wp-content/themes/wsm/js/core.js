requirejs(["modernizr", "jquery"], function(mo, $) {

	var Orpheus = {};

	Orpheus.searchScreen = function(object) {
		var defaults = {
					id: 'search-screen',
					speed: 350,
					screenOpacity: 90, // integer multiple of 10
					appendTo: 'body', // css selector
				},
				options = $.extend(defaults, object),
				overlay,
				screenInner,
				screenContent,
				form,
				html,
				$overlay;

		overlay = '<div id="'+options.id+'" class="screen screen'+options.screenOpacity+' full-screen">';
		screenInner = '<div class="screen-inner">';
		screenContent = '<div class="screen-content">';
		form = '<form role="search" method="get" class="search-form transition-all" action="'+WPVARS.siteurl+'"><label><span class="screen-reader-text">Search for:</span><i class="icon icon-search"></i><input type="search" class="search-field" value="" name="s" title="Search for:"></label><input type="submit" class="search-submit" value="Press Enter »"></form>';
		html = overlay + screenInner + screenContent + form + '</div></div></div>';

		$(options.appendTo).append(html);
		$overlay = $('#'+options.id);
		$('#'+options.id).fadeIn({
			duration: options.speed,
			done: function () {
				$('#'+options.id)
					.addClass('visible')
					.find('input[type="search"]')
						.focus();
			}
		});

		$(document).click(function(e){
			if ( e.target.localName !== 'input' ) {
				$('#'+options.id).fadeOut({
					duration: options.speed, 
					done: function() { 
						$('#'+options.id).remove(); 
					}
				});
			}
		});
	};

	Orpheus.slides = function () {

		var defaults = {
				arrows: true,
				prevArrow: '<button class="slick-prev"><i class="icon icon-arrow-left"></i></button>',
				nextArrow: '<button class="slick-next"><i class="icon icon-arrow-right"></i></button>',
				dots: true,
				autoplay: false,
				autoplaySpeed: 4000,
				fade: true,
				infinite: true,
				pauseOnHover: false,
				slide: 'li',
				slidesToScroll: 1,
				slidesToShow: 1,
				speed: 1200,
			};

		$('.carousel').each(function(){
			var $this = $(this);

			requirejs(['slick'], function(slick) {

				var settings = $this.data('settings'),
						extended = $.extend({}, defaults, settings);

				$this.slick(extended);

			});

		});
	};

	Orpheus.mediaCollection = function () {

		var $window = $(window),
			$media = $('.module-image-collection'),
			$topLast = $('.top-images img').last(),
			$bottomLast = $('.bottom-images img').last(),
			scrollSpeed = 0,
			maxSpeed = 70;

		$media
			.on('mousemove', function(event){
				var x = event.pageX,
					halfWidth = $window.width() / 2,
					distance;
				
				distance = (x - halfWidth) / halfWidth;
				scrollSpeed = maxSpeed * distance;
			})
			.hover(function() {
			    scroll();
			}, function() {
			    scrollSpeed = 0;
			});

		function scroll() {
			var sliderEdge = Math.max($topLast.position().left + $topLast.width(), $bottomLast.position().left + $bottomLast.width());
			
			if ( $media.scrollLeft() > sliderEdge - $window.width() && scrollSpeed >= 0 ) {
				scrollSpeed = 0;
			}	

			$media.animate({
			    scrollLeft: $media.scrollLeft() + scrollSpeed
			}, 100, 'linear', function() {
			    if ( scrollSpeed !== 0 ) {
			        scroll();
			    }
			});
		}

	};

	Orpheus.masonry = function () {
		$(document).ready(function(){
			$('.masonry-container').each(function(){
				var $this = $(this);

				requirejs(['masonry'], function(Masonry){

					var columnWidth = $this.data('column-width'),
							msnry = new Masonry( '.masonry-container', {
								"columnWidth": columnWidth || ".module",
								"gutter": ".gutter-sizer",
							});
				});

			});	
		});
	};
	
	Orpheus.parallax = function () {

		$('.parallax-node').each(function(){
			var $this = $(this),
					speed = parseFloat( $this.data('scroll-speed') );

			$(window).scroll(function(){
				var yPos = $(window).scrollTop() * speed,
						props = {
							webkitTransform: 'translateY(' + yPos + 'px)',
							mozTransform: 'translateY(' + yPos + 'px)',
							msTransform: 'translateY(' + yPos + 'px)',
							transform: 'translateY(' + yPos + 'px)',
						},
						props3d = {
							webkitTransform: 'translate3d(0,' + yPos + 'px,0)',
							mozTransform: 'translate3d(0,' + yPos + 'px,0)',
							msTransform: 'translate3d(0,' + yPos + 'px,0)',
							transform: 'translate3d(0,' + yPos + 'px,0)',
						};

				if ( Modernizr.csstransforms3d ) {
					$this.css(props3d);
				} else {
					$this.css(props);
				}
			});

		});
	};

	Orpheus.textEffects = function () {

		$('.scroll-fade').each(function(){
			var $this = $(this),
					speed = parseFloat( $this.data('fade-speed') );

			$(window).scroll(function(){
				var opacity = 1 - ( $(window).scrollTop() * speed );
				$this.css({
					opacity: opacity
				});
			});

		});
	};

	Orpheus.browserEvents = function () {
		var wTop,
				points = {
					minMasthead: 200
				};

		$(document)

			.on('click', '.mask .toggle-handle', function(){
				var $this = $(this),
						target = $this.data('target'),
						focus = $this.data('focus');

				$this.parents('.mask').toggle();
				$(target).toggle({
					done: function(){
						$(focus).focus();
					}
				});
				return false;
			})

			.on('click', '.menu-toggle', function(){
				var $body = $('body');

				if ($body.hasClass('primary-nav-open')) {
					$body.removeClass('primary-nav-open secondary-nav-open');
				
				} else {
					$body.addClass('primary-nav-open');
					$(document).click(function(e){
						$body.removeClass('primary-nav-open secondary-nav-open');
						$('#primary-nav li').removeClass('sub-menu-open');
					});
				}

				return false;
			})

			.on('click', '.close-submenu', function(){
				var $body = $('body');
				$body.removeClass('secondary-nav-open');
				return false;
			})

			.on('click', '#primary-nav li', function(){
				var that = $(this),
						$body = $('body'),
						submenu = that.find('.secondary-menu'),
						submenuHtml,
						submenuClass;

				if ( that.hasClass('menu-item-has-children') && !that.hasClass('sub-menu-open') ) {
					submenuHtml = submenu.html();
					submenuClass = submenu.attr('class');

					$('#primary-nav li').removeClass('sub-menu-open');
					$('#secondary-nav .menu').html(submenuHtml).attr('class', submenuClass);
					that.addClass('sub-menu-open');
					$body.addClass('secondary-nav-open');

					return false;
				
				} else if ( that.hasClass('menu-item-has-children') && that.hasClass('sub-menu-open') ) {
					return false;
				
				} else {
					$body.removeClass('secondary-nav-open primary-nav-open');
					$('#primary-nav li').removeClass('sub-menu-open');
				}
			})

			.on('click', '[href="#search"]', function(e){
				Orpheus.searchScreen();
				return false;
			})

			.on('click', '[href="#buytickets"]', function(e){
				$('.dropdown-nav-outer').slideToggle(300);
				$('body').toggleClass('dropdown-nav-open');

				$(document).click(function(e){
					$('.dropdown-nav-outer').slideUp(300);
					$('body').removeClass('dropdown-nav-open');
				});

				return false;
			})

			.on('click', '.smooth-scroll', function(e){
				var target = $(this).attr('href'),
						offset = $(target).offset().top,
						navHeight = $('.masthead-inner').outerHeight();

				$('html, body').animate({scrollTop: (offset - navHeight)+'px'});

				return false;
			})

			.on('click', '.drawer-nav > ul > li > a', function(e){
				$(this).parents('.drawer-nav').toggleClass('open');
				return false;
			})

			.on('click', '.spotify-toggle', function(e){
				e.preventDefault();
				var $recording = $(this).parent();
				$recording.toggleClass('show-spotify');
			});

		$(window)
			.scroll(function(){
				wTop = $(this).scrollTop();
				if (wTop >= points.minMasthead) {
					$('#masthead').addClass('minified');
				} else {
					$('#masthead').removeClass('minified');
				}
			});
	};

	Orpheus.recordings = function() {
		$('.no-post-thumbnail[data-spotify-id]').each(function(){
			var $this = $(this),
				$image = $this.find( '.module-image img' ),
				spotifyId = $this.data( 'spotify-id' ),
				albumArt = '',
				getAlbum = $.get( 'https://api.spotify.com/v1/albums/' + spotifyId );

				getAlbum.done(function(album) {
					if ( album.images && $(document.body).hasClass('single-recording') ) {
						albumArt = album.images[0];
					} else if ( album.images.length ) {
						if ( album.images[1] ) {
							albumArt = album.images[1];
						} else {
							albumArt = album.images[0];
						}
					}
					$image.attr( 'src', albumArt.url );
				});
		});
	};

	// Adapted from Underscore.js --> _.throttle()
	function throttle(func, wait, options) {
		var context, args, result;
		var timeout = null;
		var previous = 0;
		if (!options) options = {};
		var later = function() {
		  previous = options.leading === false ? 0 : Date.now();
		  timeout = null;
		  result = func.apply(context, args);
		  if (!timeout) context = args = null;
		};
		return function() {
		  var now = Date.now();
		  if (!previous && options.leading === false) previous = now;
		  var remaining = wait - (now - previous);
		  context = this;
		  args = arguments;
		  if (remaining <= 0 || remaining > wait) {
			if (timeout) {
			  clearTimeout(timeout);
			  timeout = null;
			}
			previous = now;
			result = func.apply(context, args);
			if (!timeout) context = args = null;
		  } else if (!timeout && options.trailing !== false) {
			timeout = setTimeout(later, remaining);
		  }
		  return result;
		};
	}

	Orpheus.parallax();
	Orpheus.textEffects();
	Orpheus.slides();
	Orpheus.masonry();
	Orpheus.recordings();
	Orpheus.browserEvents();
	if ( $('.single-media_collections .module-image-collection').length > 0 ) {
		Orpheus.mediaCollection();
	}

});